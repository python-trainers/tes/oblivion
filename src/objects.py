from trainerbase.gameobject import GameByte, GameFloat, GameUnsignedInt
from trainerbase.memory import Address
from trainerbase.process import pm

from memory import address_of_attributes, address_of_skills, last_used_item_count_address, player_coords_address


last_used_item_count = GameUnsignedInt(last_used_item_count_address)

player_x = GameFloat(player_coords_address)
player_y = GameFloat(player_coords_address + 0x4)
player_z = GameFloat(player_coords_address + 0x8)


strength = GameByte(address_of_attributes)
intelligence = GameByte(address_of_attributes + 0x1)
willpower = GameByte(address_of_attributes + 0x2)
agility = GameByte(address_of_attributes + 0x3)
speed = GameByte(address_of_attributes + 0x4)
endurance = GameByte(address_of_attributes + 0x5)
personality = GameByte(address_of_attributes + 0x6)
luck = GameByte(address_of_attributes + 0x7)


armorer = GameByte(address_of_skills + 0x0)
athletics = GameByte(address_of_skills + 0x1)
blade = GameByte(address_of_skills + 0x2)
block = GameByte(address_of_skills + 0x3)
blunt = GameByte(address_of_skills + 0x4)
hand_to_hand = GameByte(address_of_skills + 0x5)
heavy_armor = GameByte(address_of_skills + 0x6)
alchemy = GameByte(address_of_skills + 0x7)
alteration = GameByte(address_of_skills + 0x8)
conjuration = GameByte(address_of_skills + 0x9)
destruction = GameByte(address_of_skills + 0xA)
illusion = GameByte(address_of_skills + 0xB)
mysticism = GameByte(address_of_skills + 0xC)
restoration = GameByte(address_of_skills + 0xD)
acrobatics = GameByte(address_of_skills + 0xE)
light_armor = GameByte(address_of_skills + 0xF)
marksman = GameByte(address_of_skills + 0x10)
mercantile = GameByte(address_of_skills + 0x11)
security = GameByte(address_of_skills + 0x12)
sneak = GameByte(address_of_skills + 0x13)
speechcraft = GameByte(address_of_skills + 0x14)

nehrim_player_experience = GameFloat(Address(pm.base_address + 0x7A7904, [0x6E4]))
