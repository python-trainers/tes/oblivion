from trainerbase.common.teleport import Teleport, Vector3

from objects import player_x, player_y, player_z


tp = Teleport(
    player_x,
    player_y,
    player_z,
    minimal_movement_vector_length=7,
    dash_coefficients=Vector3(45, 45, 45),
)
