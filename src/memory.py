from trainerbase.memory import Address, allocate
from trainerbase.process import pm


last_used_item_count_address = Address(allocate(), [0x4])
player_coords_address = Address(pm.base_address + 0x742A14, [0x130, 0x8C])
player_stats_address = Address(pm.base_address + 0x710DDC)
address_of_attributes = player_stats_address + [0xD0]
address_of_skills = player_stats_address + [0x130]

nehrim_player_experience_address = Address(pm.base_address + 0x7A7904, [0x6E4])
