from trainerbase.codeinjection import AllocatingCodeInjection, MultipleCodeInjection
from trainerbase.process import pm

from memory import last_used_item_count_address


god_mode = AllocatingCodeInjection(
    pm.base_address + +0x25BC79,
    """
        cmp dword [esp + 0x8], 0
        jg skip
        fsub dword [esp + 0x8]

        skip:
        fadd dword [esp + 0x8]
        fstp dword [esp + 0x4]
    """,
    original_code_length=8,
)

no_weight = AllocatingCodeInjection(
    pm.base_address + 0x87E70,
    """
        mov ecx, 0
        mov [ebp + 0x8], ecx

        fcomp dword [ebp + 0x8]
        fnstsw ax
        test ah, 0x44
    """,
    original_code_length=8,
)

update_last_used_item_count_address = MultipleCodeInjection(
    AllocatingCodeInjection(
        pm.base_address + 0x906E1,
        f"""
            cmp [esp + 0x3C], edx
            jne original_label

            sub [edi + 0x4], eax
            mov [{last_used_item_count_address.base_address}], edi

            original_label:
            mov esi, eax
        """,
        original_code_length=11,
    ),
    AllocatingCodeInjection(
        pm.base_address + 0x8AB8A,
        f"""
            mov ebp, [esp + 0x34]
            add [ebx + 0x4], ebp

            mov [{last_used_item_count_address.base_address}], ebx
        """,
        original_code_length=7,
    ),
    AllocatingCodeInjection(
        pm.base_address + 0x8FB51,
        f"""
            mov eax,[esp + 0x30]
            add ecx, eax
            mov [ebp + 0x4], ecx

            mov [{last_used_item_count_address.base_address}], ebp
        """,
        original_code_length=9,
    ),
)

nothing_is_locked = AllocatingCodeInjection(
    pm.base_address + 0x28E70,
    """
        mov byte [ecx + 0x8], 0

        xor al, al
        test byte [ecx + 0x8], 0x1
    """,
    original_code_length=6,
)

one_hit_kill = AllocatingCodeInjection(
    pm.base_address + 0x25CB8E,
    """
        cmp dword [eax], 8
        jne skip
        mov dword [eax + 0x4], -999999.0

        skip:
        fld dword [eax + 0x4]
        fstp dword [esp + 0x4]
    """,
    original_code_length=7,
)
