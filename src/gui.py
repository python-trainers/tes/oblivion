from dearpygui import dearpygui as dpg
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.speedhack import SpeedHackUI
from trainerbase.gui.teleport import TeleportUI

from injections import god_mode, no_weight, nothing_is_locked, one_hit_kill
from objects import (
    acrobatics,
    agility,
    alchemy,
    alteration,
    armorer,
    athletics,
    blade,
    block,
    blunt,
    conjuration,
    destruction,
    endurance,
    hand_to_hand,
    heavy_armor,
    illusion,
    intelligence,
    last_used_item_count,
    light_armor,
    luck,
    marksman,
    mercantile,
    mysticism,
    nehrim_player_experience,
    personality,
    restoration,
    security,
    sneak,
    speechcraft,
    speed,
    strength,
    willpower,
)
from teleport import tp


@simple_trainerbase_menu("Oblivion", 720, 360)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                CodeInjectionUI(god_mode, "God Mode", "F6"),
                CodeInjectionUI(no_weight, "No Weight", "F7"),
                CodeInjectionUI(nothing_is_locked, "Nothing Is Locked", "F8"),
                CodeInjectionUI(one_hit_kill, "One Hit Kill", "F10"),
                SeparatorUI(),
                GameObjectUI(last_used_item_count, "Last Used Item Count"),
                SeparatorUI(),
                SpeedHackUI(),
            )

        with dpg.tab(label="Attributes"):
            add_components(
                GameObjectUI(strength, "Strength", default_setter_input_value=255),
                GameObjectUI(intelligence, "Intelligence", default_setter_input_value=255),
                GameObjectUI(willpower, "Willpower", default_setter_input_value=255),
                GameObjectUI(agility, "Agility", default_setter_input_value=255),
                GameObjectUI(speed, "Speed", default_setter_input_value=255),
                GameObjectUI(endurance, "Endurance", default_setter_input_value=255),
                GameObjectUI(personality, "Personality", default_setter_input_value=255),
                GameObjectUI(luck, "Luck", default_setter_input_value=255),
            )

        with dpg.tab(label="Combat Skills"):
            add_components(
                GameObjectUI(armorer, "Armorer", default_setter_input_value=100),
                GameObjectUI(athletics, "Athletics", default_setter_input_value=100),
                GameObjectUI(blade, "Blade", default_setter_input_value=100),
                GameObjectUI(block, "Block", default_setter_input_value=100),
                GameObjectUI(blunt, "Blunt", default_setter_input_value=100),
                GameObjectUI(hand_to_hand, "Hand To Hand", default_setter_input_value=100),
                GameObjectUI(heavy_armor, "Heavy Armor", default_setter_input_value=100),
            )

        with dpg.tab(label="Magic Skills"):
            add_components(
                GameObjectUI(alchemy, "Alchemy", default_setter_input_value=100),
                GameObjectUI(alteration, "Alteration", default_setter_input_value=100),
                GameObjectUI(conjuration, "Conjuration", default_setter_input_value=100),
                GameObjectUI(destruction, "Destruction", default_setter_input_value=100),
                GameObjectUI(illusion, "Illusion", default_setter_input_value=100),
                GameObjectUI(mysticism, "Mysticism", default_setter_input_value=100),
                GameObjectUI(restoration, "Restoration", default_setter_input_value=100),
            )

        with dpg.tab(label="Stealth Skills"):
            add_components(
                GameObjectUI(acrobatics, "Acrobatics", default_setter_input_value=100),
                GameObjectUI(light_armor, "Light Armor", default_setter_input_value=100),
                GameObjectUI(marksman, "Marksman", default_setter_input_value=100),
                GameObjectUI(mercantile, "Mercantile", default_setter_input_value=100),
                GameObjectUI(security, "Security", default_setter_input_value=100),
                GameObjectUI(sneak, "Sneak", default_setter_input_value=100),
                GameObjectUI(speechcraft, "Speechcraft", default_setter_input_value=100),
            )

        with dpg.tab(label="Teleport"):
            add_components(TeleportUI(tp))

        with dpg.tab(label="Nehrim"):
            add_components(GameObjectUI(nehrim_player_experience, "Experience"))
