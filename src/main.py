from trainerbase.main import run

from gui import run_menu
from injections import update_last_used_item_count_address


def on_initialized():
    update_last_used_item_count_address.inject()


if __name__ == "__main__":
    run(run_menu, on_initialized)
